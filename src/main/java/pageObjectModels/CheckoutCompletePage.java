package pageObjectModels;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutCompletePage {
	WebDriver driver;
	
	@FindBy(xpath="//div[@class='complete-text']")
	WebElement completeText;
	
	@FindBy(xpath="//h2[@class='complete-header']")
	WebElement completeHeader;
	
	
	public CheckoutCompletePage(WebDriver driver) {
		this.driver = driver;
        PageFactory.initElements(driver, this);
	}
	
	public String getCompleteOrderText() {
		return completeText.getText();
	}
	
	public String getCompleteOrderHeader() {
		return completeHeader.getText();
	}
}
