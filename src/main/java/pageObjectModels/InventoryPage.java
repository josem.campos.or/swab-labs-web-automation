package pageObjectModels;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class InventoryPage {
	WebDriver driver;
	
	@FindBy(xpath = "//a[@id='item_4_title_link']//div[@class='inventory_item_name']")
	WebElement backpack;
	
	@FindBy(id="add-to-cart-sauce-labs-backpack")
	WebElement backpackAddToCart;
	
	@FindBy(id="remove-sauce-labs-backpack")
	WebElement backpackRemove;

	@FindBy(xpath = "//a[@id='item_0_title_link']//div[@class='inventory_item_name']")
	WebElement bikeLight;
	
	@FindBy(id="add-to-cart-sauce-labs-bike-light")
	WebElement bikeLightAddToCart;

	@FindBy(id="remove-sauce-labs-bike-light")
	WebElement bikeLightRemove;
	
	@FindBy(xpath = "//a[@id='item_1_title_link']//div[@class='inventory_item_name']")
	WebElement boltTshirt;
	
	@FindBy(id="add-to-cart-sauce-labs-bolt-t-shirt")
	WebElement boltTshirtAddToCart;
	
	@FindBy(id="remove-sauce-labs-bolt-t-shirt")
	WebElement boltTshirtRemove;
	
	@FindBy(xpath = "//a[@id='item_5_title_link']//div[@class='inventory_item_name']")
	WebElement fleeceJacket;
	
	@FindBy(id="add-to-cart-sauce-labs-fleece-jacket")
	WebElement fleeceJacketAddToCart;
	
	@FindBy(id="remove-sauce-labs-fleece-jacket")
	WebElement fleeceJacketRemove;
	
	@FindBy(xpath = "//a[@id='item_2_title_link']//div[@class='inventory_item_name']")
	WebElement onesie;
	
	@FindBy(id="add-to-cart-sauce-labs-onesie")
	WebElement onesieAddToCart;
	
	@FindBy(id="remove-sauce-labs-onesie")
	WebElement onesieRemove;
	
	@FindBy(xpath = "//a[@id='item_3_title_link']//div[@class='inventory_item_name']")
	WebElement tshirtRed;
	
	@FindBy(id="add-to-cart-test.allthethings()-t-shirt-(red)")
	WebElement tshirtRedAddToCart;
	
	@FindBy(id="remove-test.allthethings()-t-shirt-(red)")
	WebElement tshirtRedRemove;
	
	@FindBy(xpath="//select[@class='product_sort_container']")
	WebElement sorter;
	
	@FindBy(xpath="//a[@class='shopping_cart_link']")
	WebElement shoppingCart;
	
	@FindBy(xpath= "//span[@class='title']")
	WebElement productsBar;
	
	@FindBy(xpath= "//img[@alt='Sauce Labs Backpack']")
	WebElement pugImage;
	
	//img[@alt='Sauce Labs Backpack']
	
	public InventoryPage(WebDriver driver) {
		this.driver = driver;
        PageFactory.initElements(driver, this);
	}
	
	public void goToBackpack() {
		backpack.click();
	}
	
	public void addBackpack() {
		backpackAddToCart.click();
	}
	
	public void removeBackpack() {
		backpackRemove.click();
	}
	
	public void goToBikeLight() {
		bikeLight.click();
	}
	
	public void addBikeLight() {
		bikeLightAddToCart.click();
	}
	
	public void removeBikeLight() {
		bikeLightRemove.click();
	}
	
	public void goToBoltTshirt() {
		boltTshirt.click();
	}
	
	public void addBoltTshirt() {
		boltTshirtAddToCart.click();
	}
	
	public void removeBoltTshirt() {
		boltTshirtRemove.click();
	}
	
	public void goToFleeceJacket() {
		fleeceJacket.click();
	}
	
	public void addFleeceJacket() {
		fleeceJacketAddToCart.click();
	}
	
	public void removeFleeceJacket() {
		fleeceJacketRemove.click();
	}
	
	public void goToOnesie() {
		onesie.click();
	}
	
	public void addOnesie() {
		onesieAddToCart.click();
	}
	
	public void removeOnesie() {
		onesieRemove.click();
	}
	
	public void goToTshirtRed() {
		tshirtRed.click();
	}
	
	public void addTshirtRed() {
		tshirtRedAddToCart.click();
	}
	
	public void removeTshirtRed() {
		tshirtRedRemove.click();
	}
	
	public void goToShoppingCart() {
		shoppingCart.click();
	}
	
	public String getProductsBarText() {
		return productsBar.getText();
	}
	
	public String getPugImageSrcText() {
		return pugImage.getAttribute("src");
	}
	
	
	public void sortProducts(String value) {
		Select sortProduct = new Select(sorter);
		sortProduct.selectByValue(value);
	}
	
}
