package pageObjectModels;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LoginPage {
	
	WebDriver driver;
	
	@FindBy(id = "user-name")
	WebElement userNameInput;
	
	@FindBy(id = "password")
	WebElement passwordInput;
	
	@FindBy(id = "login-button")
	WebElement loginButton;
	
	@FindBy(xpath="//h3[contains(text(),'Epic sadface: Sorry, this user has been locked out')]")
	WebElement errorButton;
	
	public LoginPage(WebDriver driver) {
		this.driver = driver;
        PageFactory.initElements(driver, this);
	}
	
	public void userLogin(String userName, String password) {
		userNameInput.sendKeys(userName);
		passwordInput.sendKeys(password);
	}
	public void clickOnLoginButton() {
		loginButton.click();
	}
	
	
	public String getErrorButtonText() {
		return errorButton.getText();
	}
}
