package pageObjectModels;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CartPage {
	WebDriver driver;
	WebDriverWait wait;
	
	@FindBy(id = "checkout")
	WebElement checkoutButton;
	
	@FindBy( id= "continue-shopping")
	WebElement continueShoppingButton;
	
	@FindBy(xpath = "//div[@class = 'inventory_item_name']")
	List<WebElement> productsInCart;
	
	public CartPage(WebDriver driver) {
		this.driver = driver;
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver,Duration.ofSeconds(4));
	}
	
	public void goToContinueShopping() {
		continueShoppingButton.click();
	}
	
	public void goToCheckout() {
		
		checkoutButton.click();
	}
	
	public void explicitWaitCheckoutButton() {
		wait.until(ExpectedConditions.visibilityOf(checkoutButton));
	}
	
	public void explicitWaitContinueShoppingButton() {
		wait.until(ExpectedConditions.visibilityOf(continueShoppingButton));
	}
	
	public List<String> getNamesOfElementsInCart() {
		List<String> names = new ArrayList<>();
		
		for(WebElement e : productsInCart) {
			names.add(e.getText());
		}
		return names;
	}
	
}
