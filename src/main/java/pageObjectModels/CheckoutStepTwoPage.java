package pageObjectModels;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutStepTwoPage {
	WebDriver driver;
	
	@FindBy(id="finish")
	WebElement finishButton;
	
	@FindBy(id="cancel")
	WebElement cancelButton;
	
	@FindBy(xpath = "//div[@class = 'inventory_item_name']")
	List<WebElement> productsInCart;

	public CheckoutStepTwoPage(WebDriver driver) {
		this.driver = driver;
        PageFactory.initElements(driver, this);
	}
	
	public void clickOnFinishButton() {
		finishButton.click();
	}
	
	public void clickOnCancelButton() {
		cancelButton.click();
	}
	
	public List<String> getNamesOfElementsInCart() {
		List<String> names = new ArrayList<>();
		
		for(WebElement e : productsInCart) {
			names.add(e.getText());
		}
		return names;
	}

}
