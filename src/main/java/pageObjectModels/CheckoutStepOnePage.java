package pageObjectModels;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CheckoutStepOnePage {
	WebDriver driver;
	
	@FindBy(id="first-name")
	WebElement firstNameEntry;
	
	@FindBy(id="last-name")
	WebElement lastNameEntry;
	
	@FindBy(id="postal-code")
	WebElement postalCodeEntry;
	
	@FindBy(id="cancel")
	WebElement cancelButton;
	
	@FindBy(id="continue")
	WebElement continueButton;
	
	public CheckoutStepOnePage(WebDriver driver) {
		this.driver = driver;
        PageFactory.initElements(driver, this);
	}
	
	public void fillInformation(String firstName,String lastName, String zipCode) {
		firstNameEntry.sendKeys(firstName);
		lastNameEntry.sendKeys(lastName);
		postalCodeEntry.sendKeys(zipCode);
	}
	
	public void clickOnContinueButton() {
		continueButton.click();
	}
	
	public void clickOnCancelButton() {
		cancelButton.click();
	}
	
}
