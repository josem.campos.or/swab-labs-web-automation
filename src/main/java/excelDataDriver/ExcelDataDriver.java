package excelDataDriver;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class ExcelDataDriver {
	
	public String path;
	public FileInputStream fileIn = null;
	public FileOutputStream fileOut = null;
	private XSSFWorkbook workbook = null;
	private XSSFSheet sheet = null;
	public XSSFRow row = null;
	
	public ExcelDataDriver(String path) {
		this.path = path;
		try {
			fileIn = new FileInputStream(path);
			workbook = new XSSFWorkbook(fileIn);
			sheet = workbook.getSheetAt(0);
			fileIn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int getRowCount(String sheetName) {
		int index = workbook.getSheetIndex(sheetName);
		if (index == -1)
			return 0;
		else {
			sheet = workbook.getSheetAt(index);
			int number = sheet.getLastRowNum();
			return number;
		}
	}
	
	public int getColumnCount(String sheetName)
	   {
	       sheet = workbook.getSheet(sheetName);
	       row = sheet.getRow(0);
	       int colCount = row.getLastCellNum();
	       return colCount;
	   }
	
	
	
	public ArrayList<String[]> readContentFromExcel(int columnCount,String sheetName) throws IOException{
		int index = workbook.getSheetIndex(sheetName);
		sheet = workbook.getSheetAt(index);
		
	    ArrayList<String[]> ExcelRows = new ArrayList<String[]>();
	  
	    Iterator<Row> rowIterator = sheet.iterator();
	    int cellIndex = 0;
	    while (rowIterator.hasNext()) {
	      Row row = rowIterator.next();
	      Iterator <Cell> cellIterator = row.cellIterator();
	      String[] cellValues  = new String[columnCount];
	      
	      while (cellIterator.hasNext()) {
	           Cell cell = cellIterator.next();
	           cellValues[cellIndex++] = cell.getStringCellValue();
	           
	           //System.out.print(cell.getStringCellValue() + "\t\t");
	      }
	     ExcelRows.add(cellValues);
	     cellIndex=0;
	   }
	  
	  return ExcelRows;
	}
	
}

	

