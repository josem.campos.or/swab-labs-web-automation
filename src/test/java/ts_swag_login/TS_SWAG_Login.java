package ts_swag_login;



import static org.junit.Assert.*;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjectModels.InventoryPage;
import pageObjectModels.LoginPage;


public class TS_SWAG_Login {
	WebDriver driver;
	WebDriverWait wait;
	private static Logger log = Logger.getLogger(TS_SWAG_Login.class.getName());
	
	@Before
	public void beforeMethod() {
		
		log.debug("Setting up chrome driver");
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\chromedriver.exe");
		driver = new ChromeDriver();
		
		log.debug("Maximaxing the window");
		driver.manage().window().maximize();
		
		log.debug("Navegating to the web page");
		driver.get("https://www.saucedemo.com");
	}
	
	@Test
	public void TS_SWAG_Login_Standar() throws IOException, InterruptedException 
	{
		log.info("TS_SWAG_Login_Standar TEST");
		log.debug("Initializing pom objects ");
		
		LoginPage loginPage = new LoginPage(driver);
		InventoryPage inventoryPage = new InventoryPage(driver);
		
		log.info("Entering a valid username, and valid password");
		loginPage.userLogin("standard_user", "secret_sauce");
		
		log.info("Clicking on LoginButton");
		loginPage.clickOnLoginButton();
		Thread.sleep(400);
		
		log.info("Taking Screenshot");
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("./src/test/java/ts_swag_login_screenshots/screenshotLoginStandar.png"));
		
		log.info(" Asserting on Products Banner Text");
		assertEquals(inventoryPage.getProductsBarText(),"PRODUCTS");
	}
	
	@Test
	public void TS_SWAG_Login_Locked() throws IOException, InterruptedException {
		
		log.info("TS_SWAG_Login_Locked TEST");
		log.debug("Initializing pom objects ");
		LoginPage loginPage = new LoginPage(driver);
		
		log.info("Entering a Locked username, and valid password");
		loginPage.userLogin("locked_out_user", "secret_sauce");
		
		log.info("Clicking on LoginButton");
		loginPage.clickOnLoginButton();
		Thread.sleep(400);
		
		log.info("Taking Screenshot");
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("./src/test/java/ts_swag_login_screenshots/screenshotLoginLocked.png"));
		
		log.info("Assertin on login message \"Epic sadface: Sorry, this user has been locked out.\" ");
		assertEquals(loginPage.getErrorButtonText(),"Epic sadface: Sorry, this user has been locked out.");
	
	}
	
	@Test
	public void TS_SWAG_Login_Problem() throws InterruptedException, IOException {
		
		log.info("TS_SWAG_Login_Problem TEST");
		log.debug("Initializing pom objects ");
		LoginPage loginPage = new LoginPage(driver);
		InventoryPage inventoryPage = new InventoryPage(driver);
		
		log.info("Entering a Problematic username, and valid password");
		loginPage.userLogin("problem_user", "secret_sauce");
		
		log.info("Clicking on LoginButton");
		loginPage.clickOnLoginButton();		
		Thread.sleep(400);
		
		log.info("Taking Screenshot");
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("./src/test/java/ts_swag_login_screenshots/screenshotLoginProblem.png"));
		Thread.sleep(400);
		log.info("Assert on pug image src"); 
		assertEquals(inventoryPage.getPugImageSrcText(),"https://www.saucedemo.com/static/media/sl-404.168b1cce.jpg");
	}
	
	@Test
	public void TS_SWAG_Login_Glitch() throws InterruptedException, IOException {
		
		log.info("TS_SWAG_Login_Glitch TEST");
		log.debug("Initializing pom objects ");
		LoginPage loginPage = new LoginPage(driver);
		InventoryPage inventoryPage = new InventoryPage(driver);
		
		log.info("Entering a Glitched username, and valid password");
		loginPage.userLogin("performance_glitch_user", "secret_sauce");
		
		log.info("Clicking on LoginButton");
		loginPage.clickOnLoginButton();
		
		log.info("Taking Screenshot");
		Thread.sleep(400);
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File("./src/test/java/ts_swag_login_screenshots/screenshotLoginGlitch.png"));
		
		log.info("Assert on Products Banner Text"); 
		assertEquals(inventoryPage.getProductsBarText(),"PRODUCTS");
	}
	
	@After
	public void afterMethod() {
		log.debug("Quiting driver");
		driver.quit();
	}
}
