package ts_swag_checkout;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import excelDataDriver.ExcelDataDriver;
import pageObjectModels.CartPage;
import pageObjectModels.CheckoutCompletePage;
import pageObjectModels.CheckoutStepOnePage;
import pageObjectModels.CheckoutStepTwoPage;
import pageObjectModels.InventoryPage;
import pageObjectModels.LoginPage;

@RunWith(Parameterized.class)
public class TS_SWAG_Checkout {


	WebDriver driver;
	WebDriverWait wait;
	List<String> productsListTemp;
	List<String> productsList;
	List<String> usersList;
	int testnumber;
	private static Logger log = Logger.getLogger(TS_SWAG_Checkout.class.getName());
	
	public TS_SWAG_Checkout(List<String> productsList, List<String> usersList, int testnumber) {
		this.productsList = productsList;
		this.testnumber = testnumber;
		this.usersList = usersList;
	}
	
	@Parameters
	public static  List<Object[]> testFillTheFormFeed() throws IOException{
		
		log.debug("Seting up the path for the datasample xmls file");
		String path = "./src/test/java/ts_swag_checkout_datasamples/dataSamples_TS_SWAG_Checkout.xlsx";
		ExcelDataDriver reader = new ExcelDataDriver(path);
		
		log.debug("Getting the columns and rows count");
		int columnCount = reader.getColumnCount("data");
		int rowCount = reader.getRowCount("data");
		
		log.debug("Reading the data from the xmls file");
	    ArrayList<String[]> rowsFromExcelData = reader.readContentFromExcel(columnCount,"data");
	    ArrayList<String[]> rowsFromExcelUsers = reader.readContentFromExcel(columnCount,"users");
	    
	    log.debug("Seting up the data provider object");
		Object[][] testDataFeed = new  Object[rowCount][3];
	    	  	
		log.debug("Looping throug the data");
	    for(int i=0; i < rowCount; i++) {
			testDataFeed [i][0] = Arrays.asList((rowsFromExcelData.get(i + 1)));	
			testDataFeed [i][1] = Arrays.asList((rowsFromExcelUsers.get(i + 1)));
			testDataFeed [i][2] = i;
		}
	    log.debug("Returning the data provider");
		return Arrays.asList(testDataFeed);
	}
	
	@Before
	public void beforeMethod() {
		
		log.debug("Setting up chrome driver");
		System.setProperty("webdriver.chrome.driver",
				System.getProperty("user.dir") + "\\chromedriver.exe");
		driver = new ChromeDriver();
		
		log.debug("Maximaxing the window");
		driver.manage().window().maximize();
		
		log.debug("Navegating to the web page");
		driver.get("https://www.saucedemo.com");
		
		log.debug("Creating a temporal list to compare with the products in the cart");
		productsListTemp = new ArrayList<>(productsList);
		
		log.debug("Removing null objects from the list");
		productsListTemp.removeAll(Arrays.asList("", null));
		
		log.debug("Sorting the elements of the list");
		Collections.sort(productsListTemp);
	}
	
	@Test
	public void TS_SWAG_Checkout_Test() throws IOException, InterruptedException {
		
		log.info("TS_SWAG_Cart_Test_"+ testnumber);
		
		log.debug("Initializing pom objects ");
		LoginPage loginPage = new LoginPage(driver);
		InventoryPage inventoryPage = new InventoryPage(driver);
		CartPage cartPage = new CartPage(driver);
		CheckoutStepOnePage checkoutOne = new CheckoutStepOnePage(driver);
		CheckoutStepTwoPage checkoutTwo = new CheckoutStepTwoPage(driver);
		CheckoutCompletePage checkoutComplete = new CheckoutCompletePage(driver);
		
		log.info("Entering a valid username, and valid password");
		loginPage.userLogin("standard_user", "secret_sauce");
		
		log.info("Clicking on LoginButton");
		loginPage.clickOnLoginButton();
		
		log.info("Adding the products of the list to the cart");
		if(productsList.contains("Sauce Labs Backpack")) {
			inventoryPage.addBackpack();
		}
		
		if(productsList.contains("Sauce Labs Bolt T-Shirt")) {
			inventoryPage.addBoltTshirt();
		}
		if(productsList.contains("Sauce Labs Bike Light")) {
			inventoryPage.addBikeLight();
		}
		
		if(productsList.contains("Sauce Labs Fleece Jacket")) {
			inventoryPage.addFleeceJacket();
		}
		
		if(productsList.contains("Sauce Labs Onesie")) {
			inventoryPage.addOnesie();
		}
		
		if(productsList.contains("Test.allTheThings() T-Shirt (Red)")) {
			inventoryPage.addTshirtRed();
		}
		
		log.info("Clicking on Cart button");
		inventoryPage.goToShoppingCart();
				
		log.debug("Adding all the elements from the cart to a list");
		List<String> elementsIncart = cartPage.getNamesOfElementsInCart();
		
		log.debug("Sorting the elements of the list");
		Collections.sort(elementsIncart);
		
		log.debug("Making sure the elements on the cart are correct");
		if ( !elementsIncart.equals(productsListTemp) ) {
			fail();
		}
		
		log.info("Clicking on checkoutbutton");
		cartPage.goToCheckout();
		
		log.info("Filling up the checkout form with the data provider");
		String firstName = usersList.get(0);
		String lastName = usersList.get(1);
		String postalCode = usersList.get(2);
		checkoutOne.fillInformation(firstName, lastName, postalCode);
		Thread.sleep(50);
		
		log.info("Taking screenshot");
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);	
		FileUtils.copyFile(scrFile, new File("./src/test/java/ts_swag_checkout_screenshots/screenshotCheckoutStepOne_"+ testnumber+ ".png"));
		
		log.info("Clicking on checkoutbutton");
		checkoutOne.clickOnContinueButton();
		Thread.sleep(50);
		
		log.info("Taking screenshot");
		File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);	
		FileUtils.copyFile(scrFile2, new File("./src/test/java/ts_swag_checkout_screenshots/screenshotCheckoutStepTwo_"+ testnumber+ ".png"));
		
		log.debug("Adding all the elements from the cart to a list");
		List<String> elementsInCheckout = cartPage.getNamesOfElementsInCart();
		
		log.debug("Sorting the elements of the list");
		Collections.sort(elementsInCheckout);
		
		log.debug("Making sure the elements on the cart are correct");
		if ( !elementsInCheckout.equals(productsListTemp) ) {
			fail();
		}
		
		log.info("clicking on the finish button");
		checkoutTwo.clickOnFinishButton();
		
		log.info("Asserting if  \"Thaks your for your order\" text is present");
 		assertEquals(checkoutComplete.getCompleteOrderHeader(), "THANK YOU FOR YOUR ORDER");
		Thread.sleep(50);
	}
	
	@After
	public void afterMethod() {
		log.debug("Quiting driver");
		driver.quit();
	}
	
}
