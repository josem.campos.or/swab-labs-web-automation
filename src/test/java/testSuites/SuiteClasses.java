package testSuites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import ts_swag_cart.TS_SWAG_Cart;
import ts_swag_checkout.TS_SWAG_Checkout;
import ts_swag_login.TS_SWAG_Login;

@RunWith(Suite.class)

@Suite.SuiteClasses({
	TS_SWAG_Checkout.class,
	TS_SWAG_Cart.class,
	TS_SWAG_Login.class,
	   
	})

public class SuiteClasses {

}
